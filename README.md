//Migraciones 


<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTelefonoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('telefono', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('marca');
            $table->text('Compañia');
            $table->text('telefono');
            $table->unsignedInteger('usuario_id');



        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('telefono');
    }
}





<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsuarioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usuario', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('nombre');
            $table->text('apellido_p');
            $table->text('apellido_m');
            $table->text('genero');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usuario');
    }
}







//Modelos 


<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class telefono extends Model
{
    protected $table = 'telefono';


    public function usuario()
   {	
	return $this->belongsto(usuario::class);	
   }

}




<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class usuario extends Model
{
    protected $table = 'usuario';


    public function telefono()
   {	
	return $this->hasmany(telefono::class);	
   }
   

}




//controlador


<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\usuario;

class usuariocontroller extends Controller
{
	public function Start()
	{
		$usuarios = usuario::get();
		return ('$usuarios', 'telefonos');

		
	}
    
}
